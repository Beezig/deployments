# Beezig Deployments

This repository is used to deploy and archive Beezig JAR files.

## License

[<img align="right" src="http://www.gnu.org/graphics/gplv3-127x51.png"/>][license]

Beezig is licensed under the Terms and Conditions of the **GNU GPL v3+**.<br/>
Read [Beezig's LICENSE file][license] for more.

[license]: https://gitlab.com/Beezig/Beezig/-/blob/master/LICENSE
